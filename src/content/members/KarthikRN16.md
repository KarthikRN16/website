---
name: "Karthik Ramanan"
avathar: "https://github.com/KarthikRN16.png?size=200"
designation: "Core Member"
url: "http://github.com/KarthikRN16"
dept: "CSE"
email: "karthikramanann@gmail.com"
phone: "+91 8136857336"
skills: "Coding"
---
