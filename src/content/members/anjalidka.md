---
name: "Anjali Dileepkumar"
avathar: "https://github.com/anjalidka.png?size=200"
designation: "Core Member"
url: "http://github.com/xanjali"
dept: "CSE"
email: "anjalidka@gmail.com"
phone: "+91 7034121767"
skills: "Coding"
---
